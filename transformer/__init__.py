'''
Created on 7 de jun. de 2016

@author: Jose
'''
# General imports
import os
import sys
import pkgutil
import argparse
import inspect
import logging
import importlib

# import I/O and processing modules
import reader
import writer
import processor

import reader.readerBase
import writer.writerBase
import processor.processorBase

# tools import

from transformer.tools import names


class FormatType(object):
    """
    Format Types, reader and writer
    """
    reader = 'reader'
    writer = 'writer'
    boths = [reader, writer]


def isSubModuleValid(moduleName, module):
    """
    Method that checks if the submodule is valid to be used by the transformer

    A submodule is valid if
     - has a getClass method
     - the Class returned by getClass has all the abstract methods implemented

    @ivar moduleName: submoduleName
    @type string
    @ivar module: module that contains moduleName
    @type module
    @rtype: bool
    """
    # Built full module name
    fullModuleName = module.__name__+'.'+moduleName

    # Import it
    subModule = importlib.import_module(fullModuleName)

    # check if there is the getClass method
    getClassMethodName = 'getClass'
    resulthas = hasattr(subModule, getClassMethodName)
    if not resulthas:
        # if not exists -> end
        return False

    # Chcek if the Class returned is correctly implemented
    moduleClass = getattr(subModule, getClassMethodName)()
    try:
        _ = moduleClass()
    except TypeError as ex:
        if 'abstract' in str(ex):
            return False

    # If reached here -> is valid
    return True


def getSubmodules(module, toSkip='Base', replaceName=''):
    """
    Method that gets all the valid submodules to be used by transformer

    The submodules are tested using isSubModuleValid

    The list returned will be a list of the submodules names, they are
    cleaned up by removing the value of 'replaceName' from thems

    @ivar module: module to search for valid submodules
    @type module
    @ivar toSkip: submodule prefix that shoud be skiped
    @type string
    @ivar replaceName: String that will be removed from the submodule name found
    @type string
    @rtype: list
    @see isSubModuleValid
    """
    pkgpath = os.path.dirname(module.__file__)
    submodules = list()

    for _, name, _ in pkgutil.iter_modules([pkgpath]):
        if toSkip not in name:
            submodules.append(name)

    submodulesValid = list()
    for submoduleName in submodules:
        if isSubModuleValid(submoduleName, module):
            submodulesValid.append(submoduleName)

    submodules = submodulesValid

    if replaceName:
        submodulesClean = list()
        for submodule in submodules:
            if replaceName in submodule:
                submodule = submodule.replace(replaceName, '')
                submodulesClean.append(submodule)
        submodules = submodulesClean

    return submodules


def getSupportedFormatsRead():
    readFormats = getSubmodules(reader, replaceName='reader')
    return readFormats


def getSupportedFormatsWrite():
    writeFormats = getSubmodules(writer, replaceName='writer')
    return writeFormats


def getSupportedFormats():
    """
    This method will return all the supported valid formats to be used

    It will return a dictionary using :
        - as keys the method name
        - as values the list of available formats

    @rtype: dict
    @see transformer.reader
    """
    # variable to be returned
    supportedFormats = dict()

    # we get the read and write formats avalaibles
    readFormats = getSupportedFormatsRead()
    writeFormats = getSupportedFormatsWrite()

    # built of the dictionary
    # first, assing the read formats
    for readFormat in readFormats:
        supportedFormats[readFormat] = [FormatType.reader]

    # then, add the write formats
    for writeFormat in writeFormats:
        alsoIsReader = supportedFormats.get(writeFormat)
        if alsoIsReader is None:
            supportedFormats[writeFormat] = list()
        supportedFormats[writeFormat].append(FormatType.writer)

    return supportedFormats


def getSupportedOperations():
    """
    Method that will return all the supported opreations that can be used

    It will return a list of operation names

    by default the list contains only the name 'Null'

    @rtype: list
    @see transformer.processor
    """
    return getSubmodules(processor, replaceName='processor')


class Transformer(object):
    """Transformer main Class

    It will parse the args and execute the desired command by the user

    @ivar argvToParse: list of args to be parsed
    @type list
    """
    def __init__(self, argvToParse=list()):
        self.argvToParse = argvToParse
        self.logger = logging.getLogger(names.loggerName)

    def main(self):
        """
        Main method, it will parse the args and execute the function that corresponds
        """
        self.parseArgs()
        self.execArgs()

    def parseArgs(self):
        """
        Method that will parse the args using argparse
        There are two subparses (list, transform)
        """
        # parser object
        self.parser = argparse.ArgumentParser("transformer")
        self.parser.add_argument('--version',
                                 action='version',
                                 version='0.0.1')

        # subparsers
        self.subparsers = self.parser.add_subparsers()

        self.subParseLists()
        self.subParseTransform()

        # get Args
        if self.argvToParse:
            self.args = self.parser.parse_args(self.argvToParse)
        else:
            self.args = self.parser.parse_args()

    def subParseLists(self):
        """
        Method that parses the list parameter
        """
        queryParser = self.subparsers.add_parser('list')

        queryParser.add_argument('-formats', action='store_true', default=True,
                                 help='show formats avalaible')
        queryParser.add_argument('-operations', action='store_true',
                                 default=True,
                                 help='show operations avalaible')

        queryParser.set_defaults(func=self.printList)

    def subParseTransform(self):
        """
        Method that parses the transform parameter
        """
        transformParser = self.subparsers.add_parser('transform')
        helpIfMany = ' if several, separated by commas ","'
        transformParser.add_argument('inputFile',
                                     help='input filePath[s]'+helpIfMany)
        autoDectected = ' (file extension if not especified)'
        inputFormatHelp = 'input file format' + autoDectected
        transformParser.add_argument('-inputFormat', help=inputFormatHelp,
                                     choices=getSupportedFormatsRead())

        outputFormatHelp = 'output file format' + autoDectected
        transformParser.add_argument('outputFile',
                                     help='ouput filePath[s]'+helpIfMany,
                                     )
        transformParser.add_argument('-outputFormat', help=outputFormatHelp,
                                     choices=getSupportedFormatsWrite())

        transformParser.add_argument('-operation', default="Null",
                                     help='operation to do, default is Null',
                                     required=False,
                                     choices=getSupportedOperations())
        transformParser.set_defaults(func=self.transform)

    def execArgs(self):
        """
        Method that executes the function detected by the argparser
        """
        self.args.func()

    def transform(self):
        """
        Method that executes the transform

        It will get the differents input / output formats and operation to execute

        @see Tramsformer.getInputFormat
        @see Tramsformer.getOutputFormat
        """
        # Log input info
        self.logger.info("Starting transform")
        self.logger.info("input        : {0}".format(self.args.inputFile))
        self.logger.info("output       : {0}".format(self.args.outputFile))
        self.logger.info("operation    : {0}".format(self.args.operation))

        # get Input formats
        inputFormat = self.getInputFormat()
        outPutFormat = self.getOutputFormat()

        # more log Info
        self.logger.info("inputFormat  : {0}".format(inputFormat))
        self.logger.info("outPutFormat : {0}".format(outPutFormat))

        # get format Reader
        readerClassList = self.getReaderClassList(inputFormat)
        # get format writer
        writerClassList = self.getWriterClassList(outPutFormat)

        # generate File Lists
        inputFileList = self.args.inputFile.split(',')
        outputFileList = self.args.outputFile.split(',')

        # get operation
        options = {'overwrite': True}
        processorObjClass = self.getProcessorClass(self.args.operation)
        processorObj = processorObjClass(readerClassList, inputFileList,
                                         writerClassList, outputFileList,
                                         options)
        # process the opreation
        processorObj.process()

        # end log
        self.logger.info("End transform")

    def getInputFormat(self):
        """
        Method that obtains the input format of each input file

        @see Tramsformer.getFormatFromPathList
        @rtype: list
        """
        if self.args.inputFormat:
            return self.args.inputFormat.split(',')
        return self.getFormatFromPathList(self.args.inputFile.split(','))

    def getOutputFormat(self):
        """
        Method that obtains the output format of each output file

        @see Tramsformer.getFormatFromPathList
        @rtype: list
        """
        if self.args.outputFormat:
            return self.args.outputFormat.split(',')
        return self.getFormatFromPathList(self.args.outputFile.split(','))

    def getFormatFromPathList(self, pathList):
        """
        Method that obtains the format from a list of filePaths

        @ivar pathList: list of filePath
        @type list

        @see Tramsformer.getFormatFromPath
        @rtype: list
        """
        formats = list()
        for path in pathList:
            formats.append(self.getFormatFromPath(path))
        return formats

    @staticmethod
    def getFormatFromPath(path):
        """
        Method that obtains the format from a filePath

        @ivar path: filePath
        @type str

        @return The format from a filePath, will be the extension in Uppercase
        @rtype: string
        """
        return path.split('.')[-1].upper()

    # Get Reader / Processor / Writer
    def getReaderClassList(self, shortNameList):
        """
        Method that will return the renderClass that corresponds to a list of
        formats

        @ivar shortNameList: list of formats
        @type list

        @return list of classes
        @rtype: list
        """
        readerClassList = list()
        for shortName in shortNameList:
            readerClassList.append(self.getReaderClass(shortName))
        return readerClassList

    def getWriterClassList(self, shortNameList):
        """
        Method that will return the writerClass that corresponds to a list of
        formats

        @ivar shortNameList: list of formats
        @type list

        @return list of classes
        @rtype: list
        """
        writerClassList = list()
        for shortName in shortNameList:
            writerClassList.append(self.getWriterClass(shortName))
        return writerClassList

    @staticmethod
    def getReaderClass(shortName):
        """
        Method that will return the readerClass that corresponds to the shortName format

        @ivar shortName: format name
        @type str

        @return reader Class that corresponds to the format
        @rtype: class
        """
        fullName = 'transformer.reader.reader'+shortName
        module = importlib.import_module(fullName)
        readerClass = module.getClass()
        return readerClass

    @staticmethod
    def getProcessorClass(shortName):
        """
        Method that will return the processorClass that corresponds to the shortName operation

        @ivar shortName: operation name
        @type str

        @return reader Class that corresponds to the opreation
        @rtype: class
        """
        fullName = 'transformer.processor.processor'+shortName
        module = importlib.import_module(fullName)
        processorClass = module.getClass()
        return processorClass

    @staticmethod
    def getWriterClass(shortName):
        """
        Method that will return the writerClass that corresponds to the shortName format

        @ivar shortName: format name
        @type str

        @return writer Class that corresponds to the format
        @rtype: class
        """
        fullName = 'transformer.writer.writer'+shortName
        module = importlib.import_module(fullName)
        writerClass = module.getClass()
        return writerClass

    # PRINT THINGS
    def printList(self):
        if self.args.formats:
            self.printFormats()
        if self.args.operations:
            self.printOperations()

    @staticmethod
    def printFormats():
        """
        Method that will print all the format available
        """
        supportedFormats = getSupportedFormats()

        base = '| {:<12} |{:^8}|{:^8}|'

        print '----------------------------------'
        print '|       SUPPORTED FORMATS        |'
        print '----------------------------------'
        print '| formatName   |  Read  |  Write |'
        print '----------------------------------'

        for formatName in supportedFormats:
            sf = supportedFormats[formatName]
            read = 'x' if FormatType.reader in sf else ''
            write = 'x' if FormatType.writer in sf else ''
            toPrint = base.format(formatName,
                                  read,
                                  write)
            print toPrint
        print '----------------------------------'

    @staticmethod
    def printOperations():
        """
        Method that will print all the operations available
        """
        supportedOperations = getSupportedOperations()

        base = '| {:^30} |'
        print '----------------------------------'
        print '|     SUPPORTED OPERATIONS       |'
        print '----------------------------------'

        for formatName in supportedOperations:
            toPrint = base.format(formatName)
            print toPrint
        print '----------------------------------'
