'''
Created on 7 de jun. de 2016

@author: Jose
'''
# General imports
import os
import abc
import logging

# Transformer tools imports
from transformer.tools import names
from transformer.tools import messages
from transformer.tools import processOptionsInClass


class WriterBase(object):
    """
    Basic Writer class

    It will be used as source for the severals writer formats

    in optionsInfo:
    @ivar overwrite: True / False parameter that allow the writer to overwrite
        an existing file
    @type string
    @ivar data: list of Data objects to process
    @type list
    @ivar filePath: destination file path
    @type string

    @ivar options: dictionary with all the options to be set,
                   each one will be added as local variable
    @type dict

    @ivar optionsInfo: list of tupples:
                        [varName, varDefaulfValue, varIsNeccesary]
    @type dict
    """
    __metaclass__ = abc.ABCMeta
    optionsInfo = [
                   ["overwrite", False, False],
                    ]

    def __init__(self, data, filePath, options):
        self.filePath = filePath
        self.data = data
        self.options = options
        self.logger = logging.getLogger(names.loggerName)

        self.processOptionsResult = self.__process_options__()

    def __process_options__(self):
        """
        Process Reader.options to add the diferent options needed
        @see: ReaderBase.optionsInfo
        @rtype: bool
        """
        processOk = processOptionsInClass(self)
        return processOk

    def check(self):
        """
        Does checks to verify that is possible to write the info
        Return True if every check is passed.
        @see: WriterBase.__check__
        @rtype: bool
        """
        if not self.processOptionsResult:
            return False

        # check if path is valid
        if not os.access(os.path.dirname(self.filePath), os.W_OK):
            msg = messages.Errors.notValidPath.format(self.filePath)
            self.logger.error(msg)
            return False

        # check if file exists
        if os.path.exists(self.filePath) and not self.overwrite:
            msg = messages.Errors.fileExists.format(self.filePath)
            self.logger.error(msg)
            return False

        # check if valid data
        if (not issubclass(self.data.__class__, list) or not len(self.data)):
            msg = messages.Errors.notValidData.format(self.data)
            self.logger.error(msg)
            return False

        # if all checks are valid do __check__
        return self.__check__()

    def write(self):
        if self.check():
            self.__write__()

    # --- Methods to Re implement ---
    def __check__(self):
        """
        Method that will do additional checks related to the format implemented
        This method should be re implemented if needed
        @see: WriterBase.check
        @rtype: None
        """
        msg = messages.Warnings.defaultMethod.format(self.__check__.__name__,
                                                     self.__class__.__name__)
        self.logger.warning(msg)
        return True

    @abc.abstractmethod
    def __write__(self):
        """
        Method that will write the data in filePath
        This method must be re implemented
        @see: WriterBase.write
        @rtype: None
        """
        msg = messages.Warnings.defaultMethod.format(self.__check__.__name__,
                                                     self.__class__.__name__)
        self.logger.warning(msg)
        raise Exception(msg)
