'''
Created on 8 de jun. de 2016

@author: Jose
'''

import writerBase


class WriterCSV(writerBase.WriterBase):
    """CSV Writer Class

    Writer capable of write CSV files

    in optionsInfo :
    @ivar overwrite: True / False parameter that allow the writer to overwrite
        an existing file
    @type string
    @ivar separator: character that will be use to split the lines
    @type string
    @ivar inFirstLineAreTheNames:
          indicates if the first line stores the columns name
    @type bool

    @see writerBase.WriterBase
    """
    optionsInfo = [
                   ["overwrite", False, False],
                   ["separator", ',', False],
                   ["inFirstLineAreTheNames", False, False],
                ]

    def __init__(self, data, filePath, options):
        super(WriterCSV, self).__init__(data, filePath, options)

    def __write__(self):
        names = self.getNames()
        with open(self.filePath, 'w') as fileWriter:
            toWrite = ''
            if self.inFirstLineAreTheNames:
                lineToWrite = self.generateLineFromList(names)
                toWrite += lineToWrite
            for dataLine in self.data:
                lineToWrite = self.convertDataLineIntoCsvLine(dataLine)
                toWrite += lineToWrite
            fileWriter.write(toWrite.strip())

    def getNames(self):
        """
        Method that reads the names from one Data element and stores them
        into a list that will be used as first line if the parameter is on

        The method assumes that all the data info is similar so only needs to
        get the names from the first element, if the data is not similar in
        all the Data objects it will be necessary to check all

        @rtype list
        @see WriterCSV.inFirstLineAreTheNames
        """
        firstDataLine = self.data[0]
        names = [str(data.getName()) for data in firstDataLine]
        return names

    def convertDataLineIntoCsvLine(self, dataline):
        """
        Method that builds a CSV line using as source a list of Data objects

        @ivar dataline: list of Data objects
        @type list
        @rtype list
        @see WriterCSV.generateLineFromList
        """
        values = [data.getValue() for data in dataline]
        return self.generateLineFromList(values)

    def generateLineFromList(self, listElems):
        """
        Method that builds a CSV line using as source a list of strings objects

        @ivar listElems: list of string objects
        @type list
        @rtype string
        @see WriterCSV.separator
        """
        return self.separator.join(listElems) + '\n'


def getClass():
    return WriterCSV
