'''
Created on 8 de jun. de 2016

@author: Jose
'''
import json

import writerBase


class WriterJSON(writerBase.WriterBase):
    """
    JSON Writer Class

    Writer capable of write JSON files

    @ivar overwrite: True / False parameter that allow the writer to overwrite
        an existing file

    @see writerBase.WriterBase
    """
    optionsInfo = [
                   ["overwrite", False, False],
                ]

    def __init__(self, data, filePath, options):
        super(WriterJSON, self).__init__(data, filePath, options)

    def __write__(self):
        with open(self.filePath, 'w') as fileWriter:
            jsonData = self.buildJsonData()
            json.dump(jsonData, fileWriter)

    def buildJsonData(self):
        """
        This method will build a JsonData object (basically a list of dicts)

        @rtype: list
        """
        # prepare jsonData List
        jsonDataList = list()

        for dataLine in self.data:
            # prepare jsonDataDict
            jsonDataDict = dict()

            # for each item add it to the dict
            for dataItem in dataLine:
                jsonDataDict[str(dataItem.getName())] = dataItem.getValue()

            # add dict into list
            jsonDataList.append(jsonDataDict)

        return jsonDataList


def getClass():
    return WriterJSON
