'''
Created on 7 de jun. de 2016

@author: Jose
'''
# transporter tools imports
import names
import logging


def configureLog(level=logging.INFO):
    """
    Method that will set up the transformer logger

    @ivar lvel: loggin level to set
    @type int
    @rtype: Logger
    """
    FORMAT = '%(asctime)-15s %(levelname)-7s : %(message)s'
    logging.basicConfig(format=FORMAT)

    logger = logging.getLogger(names.loggerName)
    logger.setLevel(level)
    logger.info("Logger initialized")

    return logger
