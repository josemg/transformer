'''
Created on 7 de jun. de 2016

@author: Jose
'''

import messages


def processOptionsInClass(classObj):
    """
    Process classObj.options variable to add the diferent options needed

    It will check if classObj is valid before processing the options

    It will add as variables the elements from optionsInfo
    if they are also present in the options parameter that will be their value

    if all the checks are passed, the method return True, else False

    @ivar classObj: class object that will be processed
    @see: classObj.optionsInfo
    @rtype: bool
    """

    # check if the class object has the two needs variables
    # options and optionsInfo
    if (not hasattr(classObj, "options") or
       not hasattr(classObj, "optionsInfo")):
        msg = messages.Errors.noValidObject.format(classObj.__class__.__name__)
        classObj.logger.error(msg)
        raise Exception

    # check that the options variable is a dict
    if not issubclass(classObj.options.__class__, dict):
        msg = messages.Errors.noDictFound.format("self.options")
        classObj.logger.error(msg)
        return False

    processOk = True

    # now we will parse the elements from the list optionInfo
    for optionName, defaultValue, optionIsNeed in classObj.optionsInfo:

        optionValue = classObj.options.get(optionName)

        # if the value is not present in options and is also need
        if optionValue is None and optionIsNeed:
            # an error will be logged
            # and the result will be False
            msg = messages.Errors.missionReqOption.format(optionName)
            classObj.logger.error(msg)
            processOk = False
            break
        elif optionValue is None:
            # if not we use the default value from optionsInfo as value
            optionValue = defaultValue

        # variable creation
        setattr(classObj, optionName, optionValue)

    return processOk
