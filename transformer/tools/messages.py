'''
Created on 7 de jun. de 2016

@author: Jose
'''


# Class to group the diferent error messages
class Errors(object):

    # General Errors
    errorsFound = 'Errors were found, aborting'

    # FS errors
    fileNotExists = 'The file "{0}" does not exists'
    fileExists = 'The file "{0}" already exists'
    notValidPath = 'The file path  "{0}" is not valid'

    missionReqOption = 'The required option "{0}" is missing'
    missionOption = 'The option {0} is missing'

    noDictFound = 'the variable "{0}" is not a dict'

    noValidObject = 'the object "{0}" is not valid'
    notValidData = 'the data object "{0}" is not valid'


# Class to group the diferent warning messages
class Warnings(object):
    defaultMethod = 'You are using the default method "{0}" inside "{1}"'\
                    ', please verify'
