'''
Created on 7 de jun. de 2016

@author: Jose
'''


class Data(object):
    """Basic Data class

    It will be used to store the data readed
    and also as source to the diferent writers

    There is no type limit for the parameter data

    @ivar name: name of the data
    @type string
    @ivar data: value of the data
    @type any
    """
    def __init__(self, name, value):
        self.setName(name)
        self.setValue(value)

    def setName(self, name):
        self.name = name

    def getName(self):
        return self.name

    def setValue(self, value):
        self.value = value

    def getValue(self):
        return self.value

    def __str__(self, ):
        return '{0} : {1}'.format(self.getName(),
                                  self.getValue())

    def __repr__(self, ):
        return self.__str__()

    def __cmp__(self, other):
        return cmp(self.name, other.name)
