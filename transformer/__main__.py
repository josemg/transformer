'''
Created on 8 de jun. de 2016

@author: Jose
'''
import transformer

from transformer.tools import log


def main():
    log.configureLog()
    transformerObj = transformer.Transformer()
    transformerObj.main()

if __name__ == "__main__":
    main()
