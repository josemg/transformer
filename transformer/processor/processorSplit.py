'''
Created on 9 de jun. de 2016

@author: Jose
'''
from processorBase import ProcessorBase


class ProcessorSplit(ProcessorBase):
    """
    Processor that will split in half the data from the diferent sources
    into the two first destinations

    @ivar readerClassList: list of reader Classes
    @type list
    @ivar inputFileList: list of input filePaths
    @type list

    @ivar writerClassList: list of writer Classes
    @type list
    @ivar outputFileList: list of output filePaths
    @type list

    @ivar options: options used in the Processing
    @type dict
    """
    optionsInfo = [
                   ]

    def __init__(self,
                 readerClass, inputFileList,
                 writerClass, outputFileList,
                 options=dict()):
        super(ProcessorSplit, self).__init__(readerClass, inputFileList,
                                             writerClass, outputFileList,
                                             options)

    def __check__(self):
        # at least two outFiles
        if len(self.outputFileList) < 2:
            return False
        return True

    def __process__(self):
        # get all the read data
        dataAcumulate = list()
        for index in range(len(self.inputFileList)):
            readerObj = self.getReaderObj(index)
            readerObj.read()
            dataAcumulate += readerObj.data

        half = len(dataAcumulate)/2
        dataFirstHalf = dataAcumulate[:half]
        writerObjFirst = self.getWriterObj(0, dataFirstHalf)
        writerObjFirst.write()

        dataSecondHalf = dataAcumulate[half:]
        writerObjLast = self.getWriterObj(1, dataSecondHalf)
        writerObjLast.write()


def getClass():
    return ProcessorSplit
