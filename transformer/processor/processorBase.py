'''
Created on 8 de jun. de 2016

@author: Jose
'''
# General imports
import abc
import logging
import importlib

# Transformer tools imports
from transformer.tools import messages
from transformer.tools import names
from transformer.tools import processOptionsInClass


class ProcessorBase(object):
    """Processor Base class

    It will be used as source for the diferent processing methods

    @ivar readerClassList: list of reader Classes
    @type list
    @ivar inputFileList: list of input filePaths
    @type list

    @ivar writerClassList: list of writer Classes
    @type list
    @ivar outputFileList: list of output filePaths
    @type list

    @ivar options: options used in the Processing
    @type dict
    """

    __metaclass__ = abc.ABCMeta
    optionsInfo = []

    def __init__(self,
                 readerClassList, inputFileList,
                 writerClassList, outputFileList,
                 options):
        self.logger = logging.getLogger(names.loggerName)
        self.readerClassList = readerClassList
        self.inputFileList = inputFileList
        self.writerClassList = writerClassList
        self.outputFileList = outputFileList

        self.options = options
        self.processOptionsResult = self.__process_options__()

    def __process_options__(self):
        """
        Process ProcessorBase.options to add the diferent options needed
        @see: ProcessorBase.optionsInfo
        @rtype: bool
        """
        processOk = processOptionsInClass(self)
        return processOk

    def check(self):
        """
        Does checks to verify that is possible to use a valid info
        Return True if every check is passed.
        @see: ProcessorBase.__check__
        @rtype: bool
        """
        if not self.processOptionsResult:
            return False

        # check if the parameters are valid (not None)
        paramsToCheck = [
                         self.readerClassList,
                         self.inputFileList,
                         self.writerClassList,
                         self.outputFileList,
                         ]
        for paramToCheck in paramsToCheck:
            checkOk = self.checkParam(paramToCheck)
            if not checkOk:
                return False

        # check list lenght
        for listToCheck in paramsToCheck:
            checkOk = self.checkList(listToCheck)
            if not checkOk:
                return False

        # check same lenght
        if len(self.readerClassList) != len(self.inputFileList):
            return False

        if len(self.writerClassList) != len(self.outputFileList):
            return False

        # if all checks are valid do __check__
        return self.__check__()

    def checkParam(self, paramToCheck):
        if paramToCheck is None:
            msg = messages.Errors.notValidData.format(paramToCheck)
            self.logger.error(msg)
            return False
        return True

    def checkList(self, listToCheck):
        if len(listToCheck) == 0:
            msg = messages.Errors.notValidData.format(listToCheck)
            self.logger.error(msg)
            return False
        return True

    def process(self):
        """
        Method that will do all the checks and if everything is fine
        will process the info
        @see: ProcessorBase.__read__
        @rtype: None
        """

        isValid = self.check()
        if not isValid:
            msg = messages.Errors.errorsFound
            self.logger.error(msg)
            return

        # if isValid execute the read
        self.__process__()

    # gets reader / writer
    def getReaderObj(self, index):
        if len(self.inputFileList) < index:
            return None
        return self.readerClassList[index](self.inputFileList[index], self.options)

    def getWriterObj(self, index, dataIn):
        if len(self.outputFileList) < index:
            return None
        return self.writerClassList[index](dataIn, self.outputFileList[index], self.options)

    # --- Methods to Re implement ---
    def __check__(self):
        """
        Method that will do additional checks related to the format implemented
        This method should be re implemented if needed
        @see: ProcessorBase.process
        @rtype: bool
        """
        msg = messages.Warnings.defaultMethod
        msg = msg.format(self.__check__.__name__, self.__class__.__name__)
        self.logger.warning(msg)
        return True

    @abc.abstractmethod
    def __process__(self):
        """
        Method that will process the info
        The info must be stored inside the dataOut variable
        This method must be re implemented
        @see: ProcessorBase.dataOut
        @rtype: None
        """
        msg = messages.Warnings.defaultMethod
        msg = msg.format(self.__check__.__name__, self.__class__.__name__)
        self.logger.warning(msg)
