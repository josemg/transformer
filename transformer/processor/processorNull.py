'''
Created on 8 de jun. de 2016

@author: Jose
'''
from processorBase import ProcessorBase


class ProcessorNull(ProcessorBase):
    """
    Processor that copies the data from inputFileList[0] into outputFileList[0]
    It will use the reader / writer format to make the transform

    @ivar readerClassList: list of reader Classes
    @type list
    @ivar inputFileList: list of input filePaths
    @type list

    @ivar writerClassList: list of writer Classes
    @type list
    @ivar outputFileList: list of output filePaths
    @type list

    @ivar options: options used in the Processing
    @type dict
    """
    optionsInfo = [
                   ]

    def __init__(self,
                 readerClass, inputFileList,
                 writerClass, outputFileList,
                 options=dict()):
        super(ProcessorNull, self).__init__(readerClass, inputFileList,
                                            writerClass, outputFileList,
                                            options)

    def __check__(self):
        # nothing to check, the ProcessorBase verify that there are
        # at least one input and one output
        return True

    def __process__(self):
        # get First input
        readerObj = self.getReaderObj(0)
        readerObj.read()

        # get First output
        writerObj = self.getWriterObj(0, readerObj.data)
        writerObj.write()


def getClass():
    return ProcessorNull
