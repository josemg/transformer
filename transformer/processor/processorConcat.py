'''
Created on 9 de jun. de 2016

@author: Jose
'''
from processorBase import ProcessorBase


class ProcessorConcat(ProcessorBase):
    """
    Processor that will concat the data from the diferent sources
    into the first destination

    @ivar readerClassList: list of reader Classes
    @type list
    @ivar inputFileList: list of input filePaths
    @type list

    @ivar writerClassList: list of writer Classes
    @type list
    @ivar outputFileList: list of output filePaths
    @type list

    @ivar options: options used in the Processing
    @type dict
    """
    optionsInfo = [
                   ]

    def __init__(self,
                 readerClass, inputFileList,
                 writerClass, outputFileList,
                 options=dict()):
        super(ProcessorConcat, self).__init__(readerClass, inputFileList,
                                              writerClass, outputFileList,
                                              options)

    def __check__(self):
        # nothing to check, the ProcessorBase verify that there are
        # at least one input and one output
        return True

    def __process__(self):
        # get all the read data
        dataAcumulate = list()
        for index in range(len(self.inputFileList)):
            readerObj = self.getReaderObj(index)
            readerObj.read()
            dataAcumulate += readerObj.data

        writerObj = self.getWriterObj(0, dataAcumulate)
        writerObj.write()


def getClass():
    return ProcessorConcat
