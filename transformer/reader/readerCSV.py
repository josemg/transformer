'''
Created on 7 de jun. de 2016

@author: Jose
'''
from transformer import data

from readerBase import ReaderBase


class ReaderCSV(ReaderBase):
    """CSV Reader Class

    Reader capable of read CSV files

    @ivar separator: character that will be use to split the lines
    @type string
    @ivar inFirstLineAreTheNames:
          indicates if the first line stores the columns name
    @type bool
    """
    optionsInfo = [
                   ["separator", ',', False],
                   ["inFirstLineAreTheNames", False, False],
                   ]

    def __init__(self, filePath, options=dict()):
        super(ReaderCSV, self).__init__(filePath, options)

    def __check__(self):
        # There are not addional checks
        return True

    def __read__(self):
        # get the names
        names = self.getNames()
        self.data = list()

        # process the file
        with open(self.filePath, 'r') as csvFileObj:
            startLine = 1 if names else 0
            for line in csvFileObj.read().split('\n')[startLine:]:
                lineData = self.readLineData(line, names)
                self.data.append(lineData)

    def readLineData(self, line, names):
        """
        Method that transforms a line of the csv into a Data element

        @ivar line: csv text line
        @type string
        @ivar names: list of names of each column
        @type list
        @see ReaderCSV.getNames
        """
        lineData = list()
        for index, value in enumerate(line.split(self.separator)):
            if names:
                name = names[index]
            else:
                name = None
            elementData = data.Data(name, value)
            lineData.append(elementData)
        return lineData

    def getNames(self):
        """
        Method that reads and transforms the first line of the csv
        into a Data element

        @rtype list
        @see ReaderCSV.readLineData
        """
        # if the use of names is disabled
        if not self.inFirstLineAreTheNames:
            return None

        # if not, we read the first line to get the names
        with open(self.filePath, 'r') as fileObj:
            firstLine = fileObj.read().split('\n')[0]
            names = firstLine.split(self.separator)
        return names


def getClass():
    return ReaderCSV
