'''
Created on 7 de jun. de 2016

@author: Jose
'''
# General imports
import os
import abc
import logging

# Transformer tools imports
from transformer.tools import messages
from transformer.tools import names
from transformer.tools import processOptionsInClass


class ReaderBase(object):
    """Basic Reader class

    It will be used as source for the diferent read methods

    @ivar filePath: source file path
    @type string
    @ivar options: dictionary with all the options to be set,
                   each one will be added as local variable
    @type dict

    @ivar optionsInfo: list of tupples:
                        [varName, varDefaulfValue, varIsNeccesary]
    @type dict
    """

    __metaclass__ = abc.ABCMeta
    optionsInfo = []

    def __init__(self, filePath, options=dict()):
        self.filePath = filePath
        self.options = options
        self.logger = logging.getLogger(names.loggerName)
        self.data = None

        self.processOptionsResult = self.__process_options__()

    def __process_options__(self):
        """
        Process ReaderBase.options to add the diferent options needed
        @see: ReaderBase.optionsInfo
        @rtype: bool
        """
        processOk = processOptionsInClass(self)
        return processOk

    def check(self):
        """
        Does checks to verify that is possible to read the info
        Return True if every check is passed.
        @see: ReaderBase.__check__
        @rtype: bool
        """
        if not self.processOptionsResult:
            return False
        # check if file exists
        if not os.path.exists(self.filePath):
            msg = messages.Errors.fileNotExists.format(self.filePath)
            self.logger.error(msg)
            return False
        # if all checks are valid do __check__
        return self.__check__()

    def read(self):
        """
        Method that will do all the checks and
        if everything is fine will read the info
        The read is done inside the __read__ funtion
        This method sould be re implemented in each read format
        @see: ReaderBase.__read__
        @rtype: None
        """

        isValid = self.check()
        if not isValid:
            msg = messages.Errors.errorsFound
            self.logger.error(msg)
            return

        # if isValid execute the read
        self.__read__()

    # --- Methods to Re implement ---
    def __check__(self):
        """
        Method that will do additional checks related to the format implemented
        This method should be re implemented if needed
        @see: ReaderBase.read
        @rtype: bool
        """
        msg = messages.Warnings.defaultMethod.format(self.__check__.__name__,
                                                     self.__class__.__name__)
        self.logger.warning(msg)
        return True

    @abc.abstractmethod
    def __read__(self):
        """
        Method that will read the info
        The info must be stored inside the variable 'data'
        This method must be re implemented
        @see: ReaderBase.read
        @rtype: None
        """
        msg = messages.Warnings.defaultMethod.format(self.__check__.__name__,
                                                     self.__class__.__name__)
        self.logger.warning(msg)
