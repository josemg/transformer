'''
Created on 8 de jun. de 2016

@author: Jose
'''
# General Imports
import json

# DataModel import
from transformer import data

# ReaderBase import
from readerBase import ReaderBase


class ReaderJSON(ReaderBase):
    """JSON Reader Class

    Reader capable of read JSON files

    """
    optionsInfo = [
                   ]

    def __init__(self, filePath, options=dict()):
        super(ReaderJSON, self).__init__(filePath, options)

    def __check__(self):
        # There are not addional checks
        return True

    def __read__(self):
        # Preparation of the data list
        self.data = list()

        # load file content into jsonData variable
        with open(self.filePath) as jsonFileObj:
            jsonData = json.load(jsonFileObj)

        # transform jsonData variable content into Data objects
        for jsonItemData in jsonData:
            itemDataList = list()
            for itemName, itemValue in jsonItemData.items():
                elementData = data.Data(itemName, itemValue)
                itemDataList.append(elementData)
            self.data.append(itemDataList)


def getClass():
    return ReaderJSON
