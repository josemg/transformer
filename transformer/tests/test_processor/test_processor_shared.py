# --- Processor Base ---

# -- Params --
# readerClass

# data
processorBaseValidData = dict()
processorBaseNoValidData = None

# options
processorBaseValidOptions = dict()
processorBaseNoValidOptions = 5


# --- Processor null_ns_handler
# -- Params --
# data
processorNullValidData = processorBaseValidData
# options
processorNullValidOptions = processorBaseValidOptions
