'''
Created on 8 de jun. de 2016

@author: Jose
'''
import unittest

from transformer.processor import processorNull

#from transformer.tests.test_processor import test_processor_shared

import transformer.reader.readerCSV
import transformer.reader.readerJSON
import transformer.writer.writerCSV
import transformer.writer.writerJSON


class TestProcessorNull(unittest.TestCase):

    def test_ProcessorNull_process_CSV_CSV(self):
        readerClass = [transformer.reader.readerCSV.ReaderCSV]
        inputFileList = ['transformer/tests/resources/source.csv']
        writerClass = [transformer.writer.writerCSV.WriterCSV]
        outputFileList = ['transformer/tests/resources/destinationNull.csv']
        options = {
                   'overwrite': True,
                   #'inFirstLineAreTheNames':True
                   }
        processorNullObj = processorNull.ProcessorNull(readerClass,
                                                       inputFileList,
                                                       writerClass,
                                                       outputFileList,
                                                       options)
        processorNullObj.process()

    def test_ProcessorNull_process_CSV_JSON(self):
        readerClass = [transformer.reader.readerCSV.ReaderCSV]
        inputFileList = ['transformer/tests/resources/source.csv']
        writerClass = [transformer.writer.writerJSON.WriterJSON]
        outputFileList = ['transformer/tests/resources/destinationNull.json']
        options = {
                   'overwrite': True,
                   'inFirstLineAreTheNames': True
                   }
        processorNullObj = processorNull.ProcessorNull(readerClass,
                                                       inputFileList,
                                                       writerClass,
                                                       outputFileList,
                                                       options)
        processorNullObj.process()

    def test_ProcessorNull_process_JSON_JSON(self):
        readerClass = [transformer.reader.readerJSON.ReaderJSON]
        inputFileList = ['transformer/tests/resources/source.json']
        writerClass = [transformer.writer.writerJSON.WriterJSON]
        outputFileList = ['transformer/tests/resources/destinationNull2.json']
        options = {
                   'overwrite': True,
                   'inFirstLineAreTheNames': True
                   }
        processorNullObj = processorNull.ProcessorNull(readerClass,
                                                       inputFileList,
                                                       writerClass,
                                                       outputFileList,
                                                       options)
        processorNullObj.process()

    def test_ProcessorNull_process_JSON_CSV(self):
        readerClass = [transformer.reader.readerJSON.ReaderJSON]
        inputFileList = ['transformer/tests/resources/source.json']
        writerClass = [transformer.writer.writerCSV.WriterCSV]
        outputFileList = ['transformer/tests/resources/destinationNull2.csv']
        options = {
                   'overwrite': True,
                   'inFirstLineAreTheNames': True
                   }
        processorNullObj = processorNull.ProcessorNull(readerClass,
                                                       inputFileList,
                                                       writerClass,
                                                       outputFileList,
                                                       options)
        processorNullObj.process()


if __name__ == "__main__":
    unittest.main()
