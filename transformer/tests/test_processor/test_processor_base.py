'''
Created on 8 de jun. de 2016

@author: Jose
'''
import unittest

from transformer.processor import processorBase

# from transformer.tests.test_processor import test_processor_shared

import transformer.reader.readerCSV
import transformer.writer.writerCSV


class TestProcessorBase(unittest.TestCase):
    # Quick fix to test the non abstractmethos
    processorBase.ProcessorBase.__abstractmethods__ = set()

    def test_processor_init_empty(self):
        with self.assertRaisesRegexp(TypeError, "takes exactly"):
            processorObj = processorBase.ProcessorBase()
            processorObj

    def test_processor_init_invalid_all(self):
        readerClass = None
        inputFileList = None
        writerClass = None
        outputFileList = None
        options = None
        processorObj = processorBase.ProcessorBase(readerClass,
                                                   inputFileList,
                                                   writerClass,
                                                   outputFileList,
                                                   options)
        self.assertFalse(processorObj.check(),
                         'Invalid all pass the check')

    def test_processor_init_valid_all(self):
        readerClass = [transformer.reader.readerCSV.ReaderCSV]
        inputFileList = [""]
        writerClass = [transformer.writer.writerCSV.WriterCSV]
        outputFileList = [""]
        options = dict()
        processorObj = processorBase.ProcessorBase(readerClass,
                                                   inputFileList,
                                                   writerClass,
                                                   outputFileList,
                                                   options)
        self.assertTrue(processorObj.check(),
                        'Invalid options pass the check')

if __name__ == "__main__":
    unittest.main()
