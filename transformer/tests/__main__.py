'''
Created on 9 de jun. de 2016

@author: Jose
'''
import os
import unittest


def getTransformerTestSuite():
    currentPkgPath = os.path.dirname(__file__)
    allTests = unittest.TestLoader().discover(currentPkgPath)
    return allTests


def runTestSuite():
    testRunner = unittest.TextTestRunner()
    testRunner.run(getTransformerTestSuite())


if __name__ == "__main__":
    runTestSuite()
