'''
Created on 7 de jun. de 2016

@author: Jose
'''
import unittest

import logging

from transformer import tools


class TestToolsLog(unittest.TestCase):

    def test_log_configure_log(self):
        logger = tools.log.configureLog()
        self.assertIsInstance(logger,
                              logging.Logger,
                              "No logger")


class TestToolsProcessOptions(unittest.TestCase):
    def test_processOptions_wrongClass(self):
        with self.assertRaises(Exception):
            tools.processOptionsInClass(1)

if __name__ == "__main__":
    unittest.main()
