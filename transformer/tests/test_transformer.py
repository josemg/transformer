'''
Created on 8 de jun. de 2016

@author: Jose
'''
import unittest

import transformer

from transformer import reader
from transformer import writer
from transformer import processor


class Test_Transformer_Modules(unittest.TestCase):
    def test_transformer_isSubModuleValid_validModule(self):
        moduleName = 'readerJSON'
        module = reader
        isModuleValid = transformer.isSubModuleValid(moduleName, module)
        self.assertTrue(isModuleValid,
                        'Valid module is not working')

    def test_transformer_isSubModuleValid_invalidModule(self):
        moduleName = 'readerBase'
        module = reader
        isModuleValid = transformer.isSubModuleValid(moduleName, module)
        self.assertFalse(isModuleValid,
                         'Invalid module is working')

    def test_transformer_isSubModuleValid_noExistingModule(self):
        moduleName = 'readerJJSON'
        module = reader
        with self.assertRaises(ImportError):
            transformer.isSubModuleValid(moduleName, module)

    def test_transformer_getSubModules_noArgs(self):
        with self.assertRaises(TypeError):
            transformer.getSubmodules()

    def test_transformer_getSubModules_noValidModule(self):
        module = transformer
        subModules = transformer.getSubmodules(module)
        self.assertEqual(subModules, list(),
                         "no valid module is returning submodules")

    def test_transformer_getSubModules_validModule(self):
        module = reader
        subModules = transformer.getSubmodules(module)
        self.assertNotEqual(subModules, list(),
                            "valid module is returning empty")
        subModulesExpected = ['readerCSV', 'readerJSON']
        self.assertEqual(subModules, subModulesExpected,
                         "not valid submodules data")


class Test_Transformer_formats(unittest.TestCase):
    def test_Processor_getSuportedFormats_valid_data(self):

        expectedValues = {
                         "CSV": transformer.FormatType.boths,
                         "JSON": transformer.FormatType.boths,
                         }

        gotValues = transformer.getSupportedFormats()

        msg = 'worng supported format list'
        msg += '\nexpected :{0}'
        msg += '\ngot      :{1}'

        self.assertEqual(expectedValues, gotValues,
                         msg.format(expectedValues, gotValues))

    def test_Processor_getSuportedFormats_invalid_data(self):

        expectedValues = {
                         "CSV": transformer.FormatType.reader,
                         }

        gotValues = transformer.getSupportedFormats()

        msg = 'same supported format list'
        msg += '\nexpected :{0}'
        msg += '\ngot      :{1}'

        self.assertNotEqual(expectedValues, gotValues,
                            msg.format(expectedValues, gotValues))


class Test_Transformer_operations(unittest.TestCase):
    def test_Processor_getSupportedOperations_valid_data(self):
        expectedValues = ['Concat', 'Null', 'Split']

        gotValues = transformer.getSupportedOperations()

        msg = 'worng supported opreations list'
        msg += '\nexpected : {0}'
        msg += '\ngot      : {1}'

        self.assertEqual(expectedValues, gotValues,
                         msg.format(expectedValues, gotValues))


class Test_Transformer_getElements(unittest.TestCase):
    def test_transformer_getReader(self):
        transformerObj = transformer.Transformer()
        shortName = 'CSV'
        readerClass = transformerObj.getReaderClass(shortName)

        self.assertIsNotNone(readerClass,
                             'readerClass is None')

        self.assertIsInstance(readerClass,
                              reader.readerCSV.ReaderCSV.__class__,
                              'readerClass class is incorrect')

    def test_transformer_getWriter(self):
        transformerObj = transformer.Transformer()
        shortName = 'JSON'
        writerClass = transformerObj.getWriterClass(shortName)

        self.assertIsNotNone(writerClass,
                             'writerClass is None')

        self.assertIsInstance(writerClass,
                              writer.writerJSON.getClass().__class__,
                              'writerClass class is incorrect')

    def test_transformer_getProcessor(self):
        transformerObj = transformer.Transformer()
        shortName = 'Null'
        processorClass = transformerObj.getProcessorClass(shortName)

        self.assertIsNotNone(processorClass,
                             'processorClass get is None')

        self.assertIsInstance(
                          processorClass,
                          processor.processorNull.getClass().__class__,
                          'processorClass class is incorrect')


class Test_Transformer_argParser(unittest.TestCase):
    def test_transformer_argparser_noargs(self):
        with self.assertRaises(SystemExit):
            transformerObj = transformer.Transformer()
            transformerObj.parseArgs()

    def test_transformer_argparser_invalid_args(self):
        args = ['-invalidArg']
        with self.assertRaises(SystemExit):
            transformerObj = transformer.Transformer(args)
            transformerObj.parseArgs()

    def test_transformer_argparser_valid_args_help(self):
        args = ['-h']
        with self.assertRaises(SystemExit):
            transformerObj = transformer.Transformer(args)
            transformerObj.parseArgs()

    def test_transformer_argparser_valid_args_list(self):
        args = ['list']
        transformerObj = transformer.Transformer(args)
        transformerObj.parseArgs()

    def test_transformer_argparser_valid_args_transform_null(self):
        args = ['transform',
                'transformer/tests/resources/source.json',
                'transformer/tests/resources/destinationCmd.json']
        transformerObj = transformer.Transformer(args)
        transformerObj.main()

    def test_transformer_argparser_valid_args_transform_concat(self):
        source = 'transformer/tests/resources/source_light.csv'
        destination = 'transformer/tests/resources/destinationConcat.csv'
        args = ['transform',
                source+','+source,
                destination,
                '-operation', 'Concat'
                ]
        transformerObj = transformer.Transformer(args)
        transformerObj.main()

        sourceData = open(source, 'r').read()
        destinationData = open(destination, 'r').read()

        sourceDataJoin = '\n'.join([sourceData]*2)

        self.assertEqual(sourceDataJoin,
                         destinationData,
                         'Not same content')

    def test_transformer_argparser_valid_args_transform_split(self):
        sourcejson = 'transformer/tests/resources/source_light.json'
        destinationJson = 'transformer/tests/resources/destinationSplit.json'
        destinationJson2 = destinationJson.replace('.json', '2.json')

        args = ['transform',
                sourcejson+','+sourcejson,
                destinationJson+','+destinationJson2,
                '-operation', 'Split'
                ]
        transformerObj = transformer.Transformer(args)
        transformerObj.main()

        # both destinations should be the sames
        destinationJSONdata = open(destinationJson, 'r').read()
        destinationJSONdata2 = open(destinationJson2, 'r').read()

        self.assertEqual(destinationJSONdata, destinationJSONdata2,
                         'Not same content')

if __name__ == "__main__":
    unittest.main()
