'''
Created on 8 de jun. de 2016

@author: Jose
'''
import os

from transformer.data import Data

currentDir = os.path.dirname(os.path.abspath(__file__))
resourcesDir = os.path.join(currentDir,
                            "../resources")

destinationStr = 'destination'

# Sources
realDestinationCSV = os.path.join(resourcesDir,
                                  destinationStr+".csv")

realDestinationJSON = os.path.join(resourcesDir,
                                   destinationStr+".json")


existingDestination = os.path.join(resourcesDir,
                                   'existing_'+destinationStr+".json")
realDestination = realDestinationCSV
fakeDestination = "fakePath"
wrongDestination = fakeDestination

# Options
validOptions = dict()
noValidOptions = 5

validOptionsOverwrite = {
                         "overwrite": True,
                         }

# data
validData = [
             [Data("valid", "data")]
             ]
invalidData = 5
