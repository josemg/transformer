'''
Created on 8 de jun. de 2016

@author: Jose
'''

import unittest

from transformer import tools

from transformer.reader import readerCSV
from transformer.reader import readerJSON
from transformer.writer import writerCSV
from transformer.tests.test_writer import test_writer_shared
from transformer.tests.test_reader import shared


class TestWriterCSV(unittest.TestCase):
    def test_read_csv_write_csv(self):
        readerObj = readerCSV.ReaderCSV(shared.SharedVars.realSourceCSV,
                                        shared.SharedVars.validOptionsCsvActiveFirstLine)
        readerObj.read()

        writerObj = writerCSV.WriterCSV(data=readerObj.data,
                                        filePath=test_writer_shared.realDestinationCSV,
                                        options=test_writer_shared.validOptionsOverwrite)
        writerObj.write()

    def test_read_json_write_csv(self):
        readerObj = readerJSON.ReaderJSON(shared.SharedVars.realSourceJSON,
                                          shared.SharedVars.validOptions)
        readerObj.read()

        writerObj = writerCSV.WriterCSV(data=readerObj.data,
                                        filePath=test_writer_shared.realDestinationCSV,
                                        options=test_writer_shared.validOptionsOverwrite)
        writerObj.write()


if __name__ == '__main__':
    tools.log.configureLog()
    unittest.main()
