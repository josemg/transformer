'''
Created on 8 de jun. de 2016

@author: Jose
'''
import unittest

from transformer import tools

from transformer.writer import writerBase
from transformer.tests.test_writer import test_writer_shared


class TestWriterBase(unittest.TestCase):
    # Quick fix to test the non abstractmethos
    writerBase.WriterBase.__abstractmethods__ = set()

    def test_init_writer_no_params(self):
        with self.assertRaisesRegexp(Exception, 'takes'):
            writerBase.WriterBase()

    def test_writer_process_valid_options(self):
        writerObj = writerBase.WriterBase(test_writer_shared.validData,
                                          test_writer_shared.fakeDestination,
                                          test_writer_shared.validOptions,
                                          )
        self.assertTrue(writerObj.processOptionsResult,
                        msg="Valid options are not passed")

    def test_writer_process_novalid_options(self):
        writerObj = writerBase.WriterBase(test_writer_shared.validData,
                                          test_writer_shared.fakeDestination,
                                          test_writer_shared.noValidOptions)
        self.assertFalse(writerObj.processOptionsResult,
                         msg="No valid options are passed")

    def test_writer_check_fakeDestination(self):
        writerObj = writerBase.WriterBase(test_writer_shared.validData,
                                          test_writer_shared.fakeDestination,
                                          test_writer_shared.validOptions)
        checkResult = writerObj.check()
        self.assertFalse(checkResult,
                         msg="Tried to write in a not valid path")

    def test_writer_check_goodDestination_exists_noRights(self):
        writerObj = writerBase.WriterBase(test_writer_shared.validData,
                                          test_writer_shared.existingDestination,
                                          test_writer_shared.validOptions)
        checkResult = writerObj.check()
        self.assertFalse(checkResult,
                         msg="Tried to write with no rights")

    def test_writer_check_goodDestination_exists_rights(self):
        writerObj = writerBase.WriterBase(
                                          test_writer_shared.validData,
                                          test_writer_shared.existingDestination,
                                          test_writer_shared.validOptionsOverwrite)
        self.assertTrue(writerObj.overwrite,
                        msg="The parameter overwrite is not correctly seted")

        checkResult = writerObj.check()
        self.assertTrue(checkResult,
                        msg="Check invalid and it has rights")

    def test_writer_check_invalidData(self):
        writerObj = writerBase.WriterBase(
                                          test_writer_shared.invalidData,
                                          test_writer_shared.existingDestination,
                                          test_writer_shared.validOptionsOverwrite)
        checkResult = writerObj.check()
        self.assertFalse(checkResult,
                         msg="Check is valid and there are invalid data")

    def test_writer_write_exception(self):
        writerObj = writerBase.WriterBase(test_writer_shared.validData,
                                          test_writer_shared.existingDestination,
                                          test_writer_shared.validOptionsOverwrite)
        with self.assertRaisesRegexp(Exception, 'default method'):
            writerObj.write()

if __name__ == '__main__':
    #tools.log.configureLog()
    unittest.main()
