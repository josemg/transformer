'''
Created on 7 de jun. de 2016

@author: Jose
'''
import unittest

from transformer import reader
from transformer.reader import readerBase

from shared import SharedVars


class TestReaderBaseMethods(unittest.TestCase):
    # Quick fix to test the non abstractmethos
    readerBase.ReaderBase.__abstractmethods__ = set()

    def test_init_reader_no_params(self):
        with self.assertRaisesRegexp(Exception, 'takes at least 2'):
            reader.readerBase.ReaderBase()

    def test_reader_process_valid_options(self):
        readerObj = reader.readerBase.ReaderBase(SharedVars.fakeSource,
                                                 SharedVars.validOptions)
        self.assertTrue(readerObj.__process_options__(),
                        msg="Valid options are not passed")

    def test_reader_process_novalid_options(self):
        readerObj = reader.readerBase.ReaderBase(SharedVars.fakeSource,
                                                 SharedVars.noValidOptions)
        self.assertFalse(readerObj.__process_options__(),
                         msg="No valid options are passed")

    def test_reader_check_fakeSource(self):
        readerObj = reader.readerBase.ReaderBase(SharedVars.fakeSource,
                                                 SharedVars.validOptions)
        self.assertFalse(readerObj.check(),
                         msg='Fake Source pass')

    def test_reader_check_realSource(self):
        readerObj = reader.readerBase.ReaderBase(SharedVars.realSource,
                                                 SharedVars.validOptions)
        self.assertTrue(readerObj.check(),
                        msg='Real source is not passed')

    def test_reader_read_data_Is_Not_None(self):
        readerObj = reader.readerBase.ReaderBase(SharedVars.realSource,
                                                 SharedVars.validOptions)
        self.assertIsNone(readerObj.data,
                          msg='data is not None afer read')

    def test_reader_check_should_pass(self):
        readerObj = reader.readerBase.ReaderBase(SharedVars.realSource,
                                                 SharedVars.validOptions)
        self.assertTrue(readerObj.check(),
                        msg='the data is valid and the check is false')


if __name__ == '__main__':
    #tools.log.configureLog()
    unittest.main()