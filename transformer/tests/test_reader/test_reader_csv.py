'''
Created on 7 de jun. de 2016

@author: Jose
'''
import unittest

from transformer import tools
from transformer import reader
from transformer.reader import readerCSV

from shared import SharedVars


class TestReaderCSVGeneralMethods(unittest.TestCase):

    def setUp(self):
        self.readerObj = reader.readerCSV.ReaderCSV(SharedVars.realSourceCSV,
                                                    SharedVars.validOptions)
        self.readerObj.read()

    def test_reader_csv_read_data_Is_None(self):
        self.assertIsNotNone(self.readerObj.data,
                             msg='data is None afer read')

    def test_reader_csv_read_data_Is_Data(self):
        self.assertIsInstance(self.readerObj.data,
                              list,
                              msg='data is not a valid data')

    def test_reader_csv_getNames_no_option(self):
        self.assertIsNone(self.readerObj.getNames(),
                          msg='problem in getNames if no option first line')


class TestReaderCSVDataMethods(unittest.TestCase):
    def test_reader_csv_getNames_option(self):
        self.readerObj = reader.readerCSV.ReaderCSV(SharedVars.realSourceCSV,
                                                    SharedVars.validOptionsCsvActiveFirstLine)
        self.assertIsInstance(self.readerObj.getNames(),
                              list,
                              msg='can not get names even using option')

    def test_reader_csv_getNames_validValues(self):
        self.readerObj = reader.readerCSV.ReaderCSV(SharedVars.realSourceCSV,
                                                    SharedVars.validOptionsCsvActiveFirstLine)
        names = self.readerObj.getNames()
        msg = 'csv Names not correct :\nexpected :{0}\ngot      :{1}'
        self.assertEqual(names,
                         SharedVars.csvNames,
                         msg=msg.format(SharedVars.csvNames,
                                        names)
                         )

    def test_reader_csv_read_validValues(self):
        self.readerObj = reader.readerCSV.ReaderCSV(SharedVars.realSourceCSV,
                                                    SharedVars.validOptionsCsvActiveFirstLine)
        self.readerObj.read()

    def test_reader_csv_readFirstElem_validValues(self):
        self.readerObj = reader.readerCSV.ReaderCSV(SharedVars.realSourceCSV,
                                                    SharedVars.validOptionsCsvActiveFirstLine)
        self.readerObj.read()
        got = self.readerObj.data[0]
        expected = SharedVars.fistLineDataCSV
        msg = 'line not correct :\nexpected :{0}\ngot      :{1}'

        self.assertEqual(
                         str(got),
                         str(expected),
                         msg=msg.format(expected, got)
                         )

if __name__ == "__main__":
    tools.log.configureLog()
    unittest.main()
