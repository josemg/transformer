'''
Created on 8 de jun. de 2016

@author: Jose
'''
import unittest

from transformer import tools

from transformer import reader
from transformer.reader import readerJSON

from shared import SharedVars

from transformer.tests.test_writer import test_writer_shared


class TestReaderJSONGeneralMethods(unittest.TestCase):

    def setUp(self):
        self.readerObj = reader.readerJSON.ReaderJSON(SharedVars.realSourceJSON,
                                                      SharedVars.validOptions)
        self.readerObj.read()

    def test_reader_json_read_data_Is_None(self):
        self.assertIsNotNone(self.readerObj.data,
                             msg='data is None afer read')

    def test_reader_json_read_data_Is_Data(self):
        self.assertIsInstance(self.readerObj.data,
                              list,
                              msg='data is not a valid data')


class TestReaderJSONDataMethods(unittest.TestCase):
    def test_reader_json_readFirstElem_validValues(self):
        self.readerObj = reader.readerJSON.ReaderJSON(SharedVars.realSourceJSON,
                                                      SharedVars.validOptions)
        self.readerObj.read()

        got = sorted(self.readerObj.data[0])
        expected = sorted(SharedVars.firsLineDataJSON)
        msg = 'first element is not correct :\nexpected :{0}\ngot      :{1}'
        self.assertEqual(str(got),
                         str(expected),
                         msg=msg.format(expected,
                                        got)
                         )

if __name__ == "__main__":
    tools.log.configureLog()
    unittest.main()
