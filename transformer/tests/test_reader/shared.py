'''
Created on 8 de jun. de 2016

@author: Jose
'''

import os

from transformer import data


class SharedVars(object):
    currentDir = os.path.dirname(os.path.abspath(__file__))
    resourcesDir = os.path.join(currentDir,
                                "../resources")

    # Sources
    realSourceCSV = os.path.join(resourcesDir,
                                 "source.csv")

    realSourceJSON = os.path.join(resourcesDir,
                                  "source.json")

    realSource = realSourceCSV
    fakeSource = "fakePath"

    # Options
    validOptions = dict()
    noValidOptions = 5

    # CSV
    csvNames = ["first_name", "last_name", "email", "phone_number"]
    firstLineStr = 'Jean,Williams,jwilliams0@jalbum.net,504-(819)414-0356'
    firstLine = ["Jean", "Williams",
                 "jwilliams0@jalbum.net", "504-(819)414-0356"]
    validOptionsCsvActiveFirstLine = {
                                      "inFirstLineAreTheNames": True,
                                      }
    fistLineDataCSV = [
                    data.Data(csvNames[0], firstLine[0]),
                    data.Data(csvNames[1], firstLine[1]),
                    data.Data(csvNames[2], firstLine[2]),
                    data.Data(csvNames[3], firstLine[3]),
                   ]

    #JSON 
    firstElemJSON = {
                   "first_name": "Joshua",
                   "last_name": "Rivera",
                   "email": "jrivera0@weibo.com",
                   "phone_number": "420-(256)270-9020"
                   }

    firsLineDataJSON = [
                    data.Data("first_name", firstElemJSON["first_name"]),
                    data.Data("last_name", firstElemJSON["last_name"]),
                    data.Data("email", firstElemJSON["email"]),
                    data.Data("phone_number", firstElemJSON["phone_number"]),
                   ]
