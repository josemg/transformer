'''
Created on 7 de jun. de 2016

@author: Jose
'''
from distutils.core import setup

setup(name='Transformer',
      version='0.0.1',
      description='Transfor one human readable format into another',
      author='Jose Molina',
      author_email='jose.mails@gmail.com',
      url='',
      packages=['transformer',
                'transformer.data',
                'transformer.reader',
                'transformer.writer',
                'transformer.tools',
                'transformer.processor',
                ],
)
